using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class HudController : MonoBehaviour
{
    [SerializeField] private TMP_Text _scoreText;
    [SerializeField] private GameObject _mainMenu;
    
        public void UpdateScore(int newScore)
    {
        _scoreText.text = "SCORE: " + newScore;
    }

    public void ActiveMenu(bool enable)
    {
        _mainMenu.gameObject.SetActive(enable);
    }

    public void Restart()
    {
        GameManager.Instance.RestartLevel();
    }
}

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AxeBlade : MonoBehaviour
{
    public delegate void AxeBladeHandler();

    public event AxeBladeHandler OnHit;
    
    private void OnTriggerEnter(Collider col)
    {
        if (col.TryGetComponent(out Platform currentGround))
        {
            OnHit?.Invoke();
            if (currentGround.PlatformType == PlatformType.GAME_OVER)
            {
               
                GameManager.Instance.RestartLevel();
                Debug.Log("GAME OVER");
                
            }
            else if(currentGround.PlatformType == PlatformType.FINAL_PLATFORM)
            {
                GameManager.Instance.HudController.ActiveMenu(true);
            }
            
        }
       
    }
}
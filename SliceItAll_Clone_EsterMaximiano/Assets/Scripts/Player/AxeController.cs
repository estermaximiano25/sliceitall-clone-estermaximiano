using System;
using System.Collections;
using System.Collections.Generic;
using System.Timers;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.InputSystem;

public class AxeController : MonoBehaviour
{
    [SerializeField] private GameControls _gameControls;
    
    [Header("Movement, Jump & Rotation")]
    [SerializeField] private float _jumpForce;
    [SerializeField] private float _fowardForce;
    [SerializeField] private float _rotationAngle;
    
    [Header("Components")]
    [SerializeField] private Rigidbody _rigidbody;

    [SerializeField] private AxeBlade axeBladeAxe;
    
    private bool _canMove;
    private bool _isGrounded = true;
    
    
    private void Awake()
    {
        _gameControls = new GameControls();
        _gameControls.Enable();
    }

    private void Start()
    {
        InitializeSystem();
    }
    
    private void OnDisable()
    {
        DisableSystem();
    }
    
    private void FixedUpdate()
    {
        ApplyMovement();
    }

    private void InitializeSystem()
    {
        axeBladeAxe.OnHit += StopAxeBladeAxe;
    }

    private void DisableSystem()
    {
        axeBladeAxe.OnHit -= StopAxeBladeAxe;
        _gameControls.Disable();
    }
    
    private void ApplyMovement()
    {
        if (_gameControls.Player.Jump.triggered)
        {
            Jump();
            Rotate();
        }
    }

    public void StopAxeBladeAxe()
    {
        _rigidbody.isKinematic = true;
        
        _rigidbody.position = new Vector3(transform.position.x, 
            transform.position.y + 0.1f, transform.position.z);
    }
    
    private void Jump()
    {
        _rigidbody.isKinematic = false;
        
        _rigidbody.velocity = Vector3.zero;
        
        Vector3 targetPosition = new Vector3(0,_jumpForce,_fowardForce);
        _rigidbody.AddForce(targetPosition, ForceMode.Impulse);
    }
    
    private void Rotate()
    {
        Vector3 newRotation = new Vector3(_rotationAngle, 0,0 );
        _rigidbody.AddTorque(newRotation, ForceMode.Acceleration);
    }
    
    
    
}
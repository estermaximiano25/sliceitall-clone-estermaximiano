using UnityEngine;

public class Platform : MonoBehaviour
{
    [SerializeField] private PlatformType platformType;
    public PlatformType PlatformType => platformType;
}

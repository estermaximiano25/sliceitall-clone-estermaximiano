using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    private static GameManager _instance;

    public static GameManager Instance
    {
        get { return _instance; }
    }
    
    [Header("Other Systems")]
    
    [SerializeField] private HudController _hudController;

    private void Awake()
    {
        Initialize();
    }

    private void Initialize()
    {
        if (_instance != null)
        {
            Destroy(_instance);
        }
        else
        {
            _instance = this;
        }
    }
    public void RestartLevel()
    {
        SceneManager.LoadScene("MainScene");
    }
    
    public HudController HudController => _hudController;
}

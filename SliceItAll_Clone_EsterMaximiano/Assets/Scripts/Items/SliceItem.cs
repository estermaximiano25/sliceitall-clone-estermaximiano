using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SliceItem : MonoBehaviour
{
    [SerializeField] private Rigidbody[] _rigidbodiesList;
    [SerializeField] private float _explosionForce;
    
    private void Slice()
    {
        foreach (Rigidbody rb in _rigidbodiesList)
        {
            rb.isKinematic = false;
            rb.AddExplosionForce(_explosionForce, rb.position, 130);
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            Slice();
        }
    }
}
